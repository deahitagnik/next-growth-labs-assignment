const url = "https://dummyjson.com/products?limit=100";
let products = [];
let startIndex = 0;
let debounceTimerId;      // to implement debouncing logic for delayed loading of products, when scrolled at a fast rate
const itemsPerPage = 10;
const loadingMessage = document.getElementById('loading-msg');

const fetchApiData = async () => {

    const response = await fetch(url);
    const data = await response.json();
    // console.log(data.products);

    return data.products;
}

fetchApiData();

const implementLazyLoading = () => {
     
    const endIndex = startIndex + itemsPerPage;
    const slicedProducts = products.slice(startIndex, endIndex);

    displayData(slicedProducts);

    // set the new end index as start index for subsequent function calls
    startIndex = endIndex;

    window.addEventListener('scroll', () => {

        // fetching the display messages
        const loadingMessage = document.getElementById('loading-msg');
    
        if (window.innerHeight + window.scrollY >= document.body.offsetHeight) {    // when the scroll bar hits the lower limit

            if (startIndex < products.length) {

                if (debounceTimerId)
                    clearTimeout(debounceTimerId);

                loadingMessage.style.display = "block"; // Display the loading message
    
                // delay of 1s to simulate loading of new products from api call
                setTimeout(() => {
                
                    implementLazyLoading();
                    loadingMessage.style.display = "none"; // hide the loading message display, after displaying data
                    debounceTimerId = null;
                }, 1000);

            }
        }
    });
}

const displayData = (productsData) => {

    const productGrid = document.getElementById("product-grid");

    productsData.forEach(product => {
        
        const productCard = document.createElement("div");
        productCard.classList.add("product-card");

        const productTitle = document.createElement("div");
        productTitle.classList.add("product-title");
        productTitle.textContent = product.title;

        const productImage = document.createElement("img");
        productImage.width = "200";
        productImage.height = "200";    // setting explicit width and height of images to render images quickly
        productImage.classList.add("product-image");
        productImage.src = product.thumbnail;

        const productPrice = document.createElement("div");
        productPrice.classList.add("product-price");
        productPrice.textContent = `Price: $${product.price}`;

        const productRating = document.createElement("div");
        productRating.classList.add("product-rating");
        productRating.textContent = `Rating: ${product.rating} stars`;

        productCard.appendChild(productTitle);
        productCard.appendChild(productImage);
        productCard.appendChild(productPrice);
        productCard.appendChild(productRating);

        productGrid.appendChild(productCard);
    });

}

// intial loading of products
const onLoad = async () => {
    
    products = await fetchApiData();

    const slicedProducts = products.slice(startIndex, startIndex + itemsPerPage);
    displayData(slicedProducts);
}

onLoad();

implementLazyLoading();
