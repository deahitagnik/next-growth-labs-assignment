# Click on the below link to checkout

[**LIVE PREVIEW**](https://lazy-load-nle.netlify.app/)

# Lazy Loading Application

This is a simple web application that demonstrates lazy loading of products fetched from an API. The application displays products in a grid format with a "Loading.." feature, and it incorporates debouncing logic to optimize performance.

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
- [Files and Components](#files-and-components)
- [Debouncing Logic](#debouncing-logic)

## Features

- Fetches product data from an external API (https://dummyjson.com/products).
- Displays products in a grid format with card-like styling.
- Implements lazy loading to load more products as the user scrolls down.
- Introduced a setTimeOut of 1s to intentionally simulate a delay of fetching API data.
- Uses debouncing to prevent rapid loading of products when scrolling quickly, optimising 
  performance.
- Provides a visually appealing user interface with a header and footer.

## Getting Started

Follow these steps to get the application up and running locally:

1. Clone this repository to your local machine:

   ```bash
   git clone https://github.com/your-username/lazy-loading-app.git

2. Navigate to. the project directory

    ```bash
    cd directory-name

3. Open the index.html file in your preferred web browser and start scrolling down to trigger lazy loading and observe the application's behavior.


## Files and Components

The application consists of the following files and components:

1. index.html: The main HTML file that structures the application and includes JavaScript and CSS.

2. style.css: The CSS file responsible for styling the application, including the grid layout and card-like product displays.

3. script.js: The JavaScript file that handles fetching product data, manages lazy loading, and implements debouncing logic.

4. README.md (this file): Documentation for the application, providing essential information for developers and users.

## Debouncing Logic

The debounce logic ensures that the displayProducts function is executed only after the user has stopped scrolling for the specified delay (500 ms here). This prevents rapid and unnecessary executions of the function during fast scrolling.
