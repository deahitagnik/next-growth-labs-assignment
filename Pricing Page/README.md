# Click on the link to checkout

[**LIVE PREVIEW**](https://pricing-page-nle.netlify.app/)

# Pricing Page with Slider and Modal View

This project is a simple web application that demonstrates the following features:

- Highlighting different price cards based on a slider input.
- Displaying a modal view with an overlay, on selecting a plan.
- Making a form submission when the "Proceed" button is clicked.

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
- [Usage](#usage)
- [Web vitals](#web-vitals)

## Features

### 1. Price Card Highlighting

- Three pricing plans: Basic, Pro, and Premium.
- A slider input to select a value between 0 and 30.
- The selected price card is highlighted based on the slider input:
  - 0-9: No plan highlighted.
  - 10-19: Basic plan highlighted.
  - 20-29: Pro plan is highlighted.
  - 30: Premium plan highlighted (max).

### 2. Modal View

- Clicking on a "Get started" button opens a modal view.
- The modal view is meant for getting more user details along with order comments about the pricing plan.
- An overlay is displayed behind the modal to blur the background, and focus on modal.

### 3. Form Submission

- A form with fields for name, email, and order comments.
- Clicking the "Proceed" button initiates form submission.
- JavaScript validation ensures that all fields are filled and the email is in the valid format.
- Appropriate alert messages are displayed.

## Demo

You can see a live demo of this project [here](#insert-demo-link).

## Getting Started

To run this project locally, follow these steps:

1. Clone the repository:
   ```bash
   git clone https://github.com/your-username/your-repo.git

2. Open the project directory:

    ```bash
   cd your-repo

3. Open the index.html file in your web browser to view the application.

## Usage

Adjust the slider input to select different price ranges and see the price cards highlighted accordingly.
Click on the "Get Started" button on a price card to open the modal view.
Fill out the form with your name, email, and order comments.
Click the "Proceed" button to submit the form and see the success message.

# Web vitals

![Lighthouse](https://gitlab.com/deahitagnik/next-growth-labs-assignment/-/raw/main/Pricing%20Page/assets/Screenshot_2023-09-25_at_3.24.29_PM.png?ref_type=heads)
