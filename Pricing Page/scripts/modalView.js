const modal = document.querySelector('.modal');
const overlay = document.querySelector('.overlay');
const openBtn = document.querySelectorAll('.btn-primary');  // node list of all buttons, which will trigger the modal view
const closeBtn = document.getElementById('close-btn');

const openModal = () => {
    overlay.style.display = "block";
    modal.style.display = "block";
}

const closeModal = () => {
    overlay.style.display = "none";
    modal.style.display = "none";
}

// adding event listeners to open Modal view for the targeted list of buttons
openBtn.forEach(btn => {
    btn.addEventListener('click', () => {
        console.log('clicked');
        openModal();
    });
});

// adding event listeners to close Modal view on clicking on overlay view (anywhere other than Modal view) and cancel button
overlay.addEventListener('click', closeModal);
closeBtn.addEventListener('click', closeModal);
