const proceedBtn = document.getElementById('proceed-btn');
const cancelBtn = document.getElementById('cancel-btn');
const formEle = document.getElementById('form-data');

const validateData = (fname, lname, email) => {

    const emailPatternTest = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9-]+\.[a-zA-Z]{2,4}$/;

    if (!fname) {
        alert("Please enter first name");
        return false;
    }

    if (!lname) {
        alert("Please enter last name");
        return false;
    }

    if (!email) {
        alert("Please enter email");
        return false;
    }

    if (!emailPatternTest.test(email)) {
        alert('Please enter a valid email address');
        return false;
    }
    return true;
}

const handleFormSubmit = () => {

    const firstName = document.getElementById('fname').value;
    const lastName = document.getElementById('lname').value;
    const email = document.getElementById('email').value;

    // console.log(firstName, lastName, email);

    if (validateData(firstName, lastName, email)) {
        // if all validations pass, then post the data to the URL, by calling form.submit()
        const a = formEle.submit();
        document.querySelector('.modal').style.display = "none";
        document.querySelector('.overlay').style.display = "none";
    }
}

proceedBtn.addEventListener('click', () => {
    handleFormSubmit();
});

cancelBtn.addEventListener('click', () => {
    console.log('cancel clicked');
    document.querySelector('.modal').style.display = "none";
    document.querySelector('.overlay').style.display = "none";
});
