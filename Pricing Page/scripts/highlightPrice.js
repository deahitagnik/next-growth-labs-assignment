const slider = document.getElementById('slider');
const price = document.getElementById('price');

// selecting the buttons for different plans
const basicPlan = document.getElementById('basic-plan');
const proPlan = document.getElementById('pro-plan');
const premiumPlan = document.getElementById('premium-plan');

slider.addEventListener('input', (event) => {
    const selectedPrice = event.target.value;
    price.textContent = `$${selectedPrice}`;
    
    if (selectedPrice <= 10) {
        basicPlan.classList.remove('highlight-btn');
        proPlan.classList.remove('highlight-btn');
        premiumPlan.classList.remove('highlight-btn');
    }

    else if (selectedPrice > 10 && selectedPrice <= 19) {
        basicPlan.classList.add('highlight-btn');
        proPlan.classList.remove('highlight-btn');
        premiumPlan.classList.remove('highlight-btn');
    }

    else if (selectedPrice >= 20 && selectedPrice < 30) {
        basicPlan.classList.remove('highlight-btn');
        proPlan.classList.add('highlight-btn');
        premiumPlan.classList.remove('highlight-btn');
    }
        
    else {  // when selected price > 30
        basicPlan.classList.remove('highlight-btn');
        proPlan.classList.remove('highlight-btn');
        premiumPlan.classList.add('highlight-btn');
    }

})